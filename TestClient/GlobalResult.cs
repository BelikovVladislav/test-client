﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestClient.utilites;

namespace TestClient
{
    public partial class GlobalResult : Form
    {
        public GlobalResult()
        {
            InitializeComponent();
        }

        private void печатьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var pd = new PrintDocument();
            pd.PrintPage += (s, eventArgs) =>
            {
                var bmp = new Bitmap(statisticDGV.Width, statisticDGV.Height);
                statisticDGV.DrawToBitmap(bmp, statisticDGV.ClientRectangle);
                eventArgs.Graphics.DrawImage(bmp, new Point(100, 100));
            };
            pd.Print();
        }
        private void syncStatistic()
        {
            var statistic = Server.GetInstance().GetStatistic((string)periodCB.SelectedItem);
            statisticDGV.Rows.Clear();
            statisticDGV.Rows.Add(statistic.period, statistic.testCount, statistic.passedCount, statistic.percent);
        }
        private void GlobalResult_Load(object sender, EventArgs e)
        {
            periodCB.SelectedIndex = 0;
            syncStatistic();
        }

        private void periodCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            syncStatistic();
        }
    }
}
