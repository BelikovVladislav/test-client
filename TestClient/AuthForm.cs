﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestClient.utilites;
namespace TestClient
{
    public partial class AuthForm : Form
    {
        public AuthForm()
        {
            InitializeComponent();
        }

        private void EnterButton_Click(object sender, EventArgs e)
        {
            try
            {
                Server.GetInstance().Connect("localhost:3000").IsTeacher = teacherCheckbox.Checked;
                CategoryForm categoryForm = new CategoryForm();
                categoryForm.FormClosed += CategoryForm_FormClosed;
                Hide();
                categoryForm.Show();
            }
            catch (Exception ex)
            {
            }
        }

        private void CategoryForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Show();
        }

        private void HideTooltip(object sender, KeyEventArgs args) 
        {
           // infoTooltip.Hide(ipTextBox);
        }
        private void AuthForm_Load(object sender, EventArgs e)
        {
          //  ipTextBox.KeyDown += new KeyEventHandler(HideTooltip);
        }
    }
}
