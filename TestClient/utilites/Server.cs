﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestClient.Models;
using RestSharp;

namespace TestClient.utilites
{
    class Server
    {
        private RestClient restClient = null;
        private static Server server;
        public bool IsTeacher { get; set; } = false;
        public static Server GetInstance()
        {
            if (server == null)
            {
                server = new Server();
            }
            return server;
        }

        private Server() { }
        public Server Connect(string ip = null)
        {
            if (ip == null)
            {
                throw new Exception("Введите ip");
            }
            var http = "http://";
            ip = (ip.IndexOf(http) == -1 ? http : "") + ip;
            restClient = new RestClient(ip);
            var response = restClient.Get(new RestRequest("check"));
            if (response.Content != "ok") 
            {
                throw new Exception("No Response");
            }
            
            return this;
        }

        public List<Category> GetCategories() 
        {
            var request = new RestRequest("categories");
            return restClient.Get<List<Category>>(request).Data;
        }

        public Category CreateCategory(Category category)
        {
            var request = new RestRequest("category");
            request.AddJsonBody(category);
            var response = restClient.Post<Category>(request);
            return response.Data;
        }

        public bool CheckCategoryExists(string name) 
        {
            var request = new RestRequest("check-category-exists/{name}");
            request.AddUrlSegment("name", name);
            return restClient.Get<bool>(request).Data;
        }


        public string SaveTest(Test test)
        {
            var requst = new RestRequest("test/{categoryID}");
            requst.AddUrlSegment("categoryID", test.categoryID);
            requst.AddJsonBody(test);
            return restClient.Post(requst).Content;

        }

        public List<Test> GetTests(string categoryID)
        {
            var request = new RestRequest("tests/{categoryID}");
            request.AddUrlSegment("categoryID", categoryID);
            return restClient.Get<List<Test>>(request).Data;
        }

        public Test GetTest(string testID)
        {
            var request = new RestRequest("test/{testID}");
            request.AddUrlSegment("testID", testID);
            return restClient.Get<Test>(request).Data;
        }

        public List<Models.File> GetFiles()
        {
            var request = new RestRequest("files");
            return restClient.Get<List<Models.File>>(request).Data;
        }

        public Models.File UploadFile(string path, string name)
        {
            var requset = new RestRequest("file");
            requset.AddFile(name, path);
            return restClient.Post<Models.File>(requset).Data;
        }

        public void RemoveFile(Models.File file)
        {
            var request = new RestRequest("file/{id}");
            request.AddUrlSegment("id", file._id);
            restClient.Delete(request);
        }

        public void OpenFile(Models.File file)
        {
            var request = new RestRequest("file/{name}");
            request.AddUrlSegment("name", file.fileName);
            var bytes = restClient.DownloadData(request);
            var path = Environment.GetEnvironmentVariable("USERPROFILE") + @"\" + "Downloads" + @"\" + file.originalFileName;
            System.IO.File.WriteAllBytes(path, bytes);
            System.Diagnostics.Process.Start(path);
        }

        public void SaveStatistic(StudentStatistic studentStatistic)
        {
            var request = new RestRequest("statistic");
            request.AddJsonBody(studentStatistic);
            restClient.Post(request);
        }

        public GlobalStatistic GetStatistic(string period)
        {
            var request = new RestRequest("statistic");
            request.AddParameter("period", period);
            return restClient.Get<GlobalStatistic>(request).Data;
        }
    }
}
