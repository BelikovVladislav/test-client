﻿namespace TestClient
{
    partial class FIleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FIleForm));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.filesBox = new System.Windows.Forms.ListBox();
            this.uploadButton = new System.Windows.Forms.Button();
            this.removeFileButton = new System.Windows.Forms.Button();
            this.selectFile = new System.Windows.Forms.OpenFileDialog();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 84.54546F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.45455F));
            this.tableLayoutPanel1.Controls.Add(this.filesBox, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.uploadButton, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.removeFileButton, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 91.79955F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.200456F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(330, 439);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // filesBox
            // 
            this.filesBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.filesBox.FormattingEnabled = true;
            this.filesBox.ItemHeight = 16;
            this.filesBox.Location = new System.Drawing.Point(3, 3);
            this.filesBox.Name = "filesBox";
            this.filesBox.Size = new System.Drawing.Size(272, 397);
            this.filesBox.TabIndex = 1;
            this.filesBox.SelectedIndexChanged += new System.EventHandler(this.filesBox_SelectedIndexChanged);
            this.filesBox.DoubleClick += new System.EventHandler(this.filesBox_DoubleClick);
            // 
            // uploadButton
            // 
            this.uploadButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uploadButton.Location = new System.Drawing.Point(3, 406);
            this.uploadButton.Name = "uploadButton";
            this.uploadButton.Size = new System.Drawing.Size(272, 30);
            this.uploadButton.TabIndex = 0;
            this.uploadButton.Text = "Загрузить";
            this.uploadButton.UseVisualStyleBackColor = true;
            this.uploadButton.Click += new System.EventHandler(this.uploadButton_Click);
            // 
            // removeFileButton
            // 
            this.removeFileButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.removeFileButton.Location = new System.Drawing.Point(281, 406);
            this.removeFileButton.Name = "removeFileButton";
            this.removeFileButton.Size = new System.Drawing.Size(46, 30);
            this.removeFileButton.TabIndex = 2;
            this.removeFileButton.Text = "X";
            this.removeFileButton.UseVisualStyleBackColor = true;
            this.removeFileButton.Click += new System.EventHandler(this.removeFileButton_Click);
            // 
            // FIleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(330, 439);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FIleForm";
            this.Text = "Файлы";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button uploadButton;
        private System.Windows.Forms.ListBox filesBox;
        private System.Windows.Forms.Button removeFileButton;
        private System.Windows.Forms.OpenFileDialog selectFile;
    }
}