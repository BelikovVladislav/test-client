﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestClient.Models;
using TestClient.utilites;

namespace TestClient
{
    public partial class AddTestForm : Form
    {
        private Test test;
        public AddTestForm()
        {
            InitializeComponent();
        }

        public AddTestForm(Test test) : this ()
        {
            this.test = test;
        }

        private void syncQuestions()
        {
            questionsBox.Items.Clear();
            questionsBox.Items.AddRange(test.questions.ToArray());
            syncAnswers();
        }

        private void syncAnswers()
        {
            answersBox.Items.Clear();
            deleteQuestion.Enabled = addAnswerButton.Enabled = false;
            if (questionsBox.SelectedItem != null)
            {
                deleteQuestion.Enabled = addAnswerButton.Enabled = true;
                var question = (Question)questionsBox.SelectedItem;
                addAnswerButton.Enabled = !(question.isClosed && question.answers.Count > 0);
                answersBox.Items.AddRange(question.answers.ToArray());
            }
        }

        private void AddTestForm_Load(object sender, EventArgs e)
        {
            syncQuestions();
            deleteQuestion.Enabled = deleteAnswer.Enabled = addAnswerButton.Enabled = false;
            Text = "Создание: " + test.name;
        }

        private void questionsBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            syncAnswers();
        }

        private void deleteAnswer_Click(object sender, EventArgs e)
        {
            test.questions[questionsBox.SelectedIndex].RemoveAnswer(answersBox.SelectedIndex);
            syncAnswers();
        }

        private void answersBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            deleteAnswer.Enabled = answersBox.SelectedItem != null;
        }

        private void deleteQuestion_Click(object sender, EventArgs e)
        {
            test.RemoveQuestion(questionsBox.SelectedIndex);
            syncQuestions();
        }

        private void addQuestionButton_Click(object sender, EventArgs e)
        {
            var question = new Question();
            var addQuestionForm = new AddQuestionForm(question);
            addQuestionForm.ShowDialog();
            if (question.name != null) 
            {
                test.AddQuestion(question);
                syncQuestions();
            }
        }

        private void addAnswerButton_Click(object sender, EventArgs e)
        {
            var answer = new Answer();
            var question = (Question)questionsBox.SelectedItem;
            var addAnswerForm = new AddAnswerForm(answer, question.name);
            addAnswerForm.ShowDialog();
            if (answer.name != null)
            {
                if (question.isClosed) 
                {
                    answer.isTrue = true;
                }
                question.AddAnswer(answer);
                syncAnswers();
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (test.IsValid)
            {
                var id = Server.GetInstance().SaveTest(test);
                test._id = id;
                Close();
            }
            else
            {
                MessageBox.Show("Вы допустили ошибки при создании теста, пожалуйста перепроверьте");
            }
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
