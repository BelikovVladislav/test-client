﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestClient.Models;
using TestClient.utilites;

namespace TestClient
{
    public partial class AddAnswerForm : Form
    {
        private Answer answer;
        public AddAnswerForm()
        {
            InitializeComponent();
        }

        public AddAnswerForm(Answer answer, string questionName) : this()
        {
            this.answer = answer;
            questionLabel.Text = questionName;
        }
        private void AddCategoryForm_Load(object sender, EventArgs e)
        {
            nameTextBox.Text = "";
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            if (nameTextBox.Text != "")
            {
                answer.name = nameTextBox.Text;
                answer.isTrue = isTrueAnswer.Checked;
                Close();
            }
            else
            {
                MessageBox.Show("Введите ответ");
            }

        }
    }
}
