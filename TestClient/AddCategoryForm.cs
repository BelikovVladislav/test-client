﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestClient.Models;
using TestClient.utilites;

namespace TestClient
{
    public partial class AddCategoryForm : Form
    {
        public AddCategoryForm()
        {
            InitializeComponent();
        }

        private void AddCategoryForm_Load(object sender, EventArgs e)
        {
            nameTextBox.Text = "";
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            var category = new Category();
            category.name = nameTextBox.Text;
            if (Server.GetInstance().CheckCategoryExists(category.name)) 
            {
                MessageBox.Show("Категория с таким названием уже существует!");
                return;
            }
            Server.GetInstance().CreateCategory(category);
            Close();
        }
    }
}
