﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestClient.utilites;

namespace TestClient
{
    public partial class FIleForm : Form
    {
        public FIleForm()
        {
            InitializeComponent();
            filesBox.Items.AddRange(Server.GetInstance().GetFiles().ToArray());
            uploadButton.Enabled = Server.GetInstance().IsTeacher;
            removeFileButton.Enabled = false;
        }

        private void uploadButton_Click(object sender, EventArgs e)
        {
            if (selectFile.ShowDialog() == DialogResult.Cancel) { return; }
            var path = selectFile.FileName;
            var name = path.Split('\\').Last<string>();
            var file = Server.GetInstance().UploadFile(path, name);
            filesBox.Items.Add(file);
        }

        private void filesBox_DoubleClick(object sender, EventArgs e)
        {
            if (filesBox.SelectedItem != null)
            {
                var file = (Models.File)filesBox.SelectedItem;
                Server.GetInstance().OpenFile(file);
            }
        }

        private void filesBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            removeFileButton.Enabled = filesBox.SelectedItem != null && Server.GetInstance().IsTeacher;
        }

        private void removeFileButton_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Вы уверены что хотите удалить файл?", "Вы уверены", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Server.GetInstance().RemoveFile(filesBox.SelectedItem as Models.File);
                filesBox.Items.Clear();
                filesBox.Items.AddRange(Server.GetInstance().GetFiles().ToArray());
                removeFileButton.Enabled = false;
            }
        }
    }
}
