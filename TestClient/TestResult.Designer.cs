﻿namespace TestClient
{
    partial class TestResult
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TestResult));
            this.testStatistic = new System.Windows.Forms.DataGridView();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.печатьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.countTrues = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.countFalse = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.precent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.passed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.testStatistic)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // testStatistic
            // 
            this.testStatistic.AllowUserToAddRows = false;
            this.testStatistic.AllowUserToDeleteRows = false;
            this.testStatistic.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.testStatistic.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.name,
            this.countTrues,
            this.countFalse,
            this.precent,
            this.passed});
            this.testStatistic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.testStatistic.Location = new System.Drawing.Point(0, 28);
            this.testStatistic.Name = "testStatistic";
            this.testStatistic.ReadOnly = true;
            this.testStatistic.RowHeadersWidth = 51;
            this.testStatistic.RowTemplate.Height = 24;
            this.testStatistic.Size = new System.Drawing.Size(679, 422);
            this.testStatistic.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.печатьToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(679, 28);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // печатьToolStripMenuItem
            // 
            this.печатьToolStripMenuItem.Name = "печатьToolStripMenuItem";
            this.печатьToolStripMenuItem.Size = new System.Drawing.Size(72, 24);
            this.печатьToolStripMenuItem.Text = "Печать";
            this.печатьToolStripMenuItem.Click += new System.EventHandler(this.печатьToolStripMenuItem_Click);
            // 
            // name
            // 
            this.name.HeaderText = "Тест";
            this.name.MinimumWidth = 6;
            this.name.Name = "name";
            this.name.ReadOnly = true;
            this.name.Width = 125;
            // 
            // countTrues
            // 
            this.countTrues.HeaderText = "Количество верных ответов";
            this.countTrues.MinimumWidth = 6;
            this.countTrues.Name = "countTrues";
            this.countTrues.ReadOnly = true;
            this.countTrues.Width = 125;
            // 
            // countFalse
            // 
            this.countFalse.HeaderText = "Количество неверных ответов";
            this.countFalse.MinimumWidth = 6;
            this.countFalse.Name = "countFalse";
            this.countFalse.ReadOnly = true;
            this.countFalse.Width = 125;
            // 
            // precent
            // 
            this.precent.HeaderText = "Процент";
            this.precent.MinimumWidth = 6;
            this.precent.Name = "precent";
            this.precent.ReadOnly = true;
            this.precent.Width = 125;
            // 
            // passed
            // 
            this.passed.HeaderText = "Сдано";
            this.passed.MinimumWidth = 6;
            this.passed.Name = "passed";
            this.passed.ReadOnly = true;
            this.passed.Width = 125;
            // 
            // TestResult
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(679, 450);
            this.Controls.Add(this.testStatistic);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "TestResult";
            this.Text = "Результаты теста";
            this.Load += new System.EventHandler(this.TestResult_Load);
            ((System.ComponentModel.ISupportInitialize)(this.testStatistic)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView testStatistic;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem печатьToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn countTrues;
        private System.Windows.Forms.DataGridViewTextBoxColumn countFalse;
        private System.Windows.Forms.DataGridViewTextBoxColumn precent;
        private System.Windows.Forms.DataGridViewTextBoxColumn passed;
    }
}