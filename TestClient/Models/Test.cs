﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestClient.Models
{
    public class Test
    {
        public Test()
        {
            questions = new List<Question>();
        }
        public string _id { get; set; }
        public string name { get; set; }

    //    public Question[] questions { get => listQuestions.ToArray(); set { listQuestions.Clear(); listQuestions.AddRange(value); } }
        public List<Question> questions { get; set; }

        public bool IsValid { get => questions.Aggregate(true, (acc, x) => acc && x.IsValid); }

        public string categoryID { get; set; }
        public void AddQuestion(Question question)
        {
            if (questions == null)
            {
                questions = new List<Question>();
            }
            questions.Add(question);
        }
        public void RemoveQuestion(int index)
        {
            questions.RemoveAt(index);
        }
        public override string ToString()
        {
            return this.name;
        }
    }
}