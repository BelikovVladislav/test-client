﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestClient.Models
{
    public class StudentStatistic
    {

        public Test test { get; set; }

        public int true_questions_count { get; set; }
        public int false_questions_count { get => test.questions.Count - true_questions_count; }

        public double percent
        {
            get
            {
                var count = test.questions.Count;
                return (true_questions_count / count) * 100;
            }
        }

        public bool passed 
        {
            get => percent > 85;                
        }

        public DateTime dateTime { get; set; } = DateTime.Now;
    }
}
