﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestClient.Models
{
    class GlobalStatistic
    {
        public string period { get; set; }

        public int testCount { get; set; }

        public int passedCount { get; set; }

        public double percent { get; set; }
    }
}
