﻿

using System.Data.Entity;

namespace TestBackend.Models.Contexts
{
    public class AnswerContext: DbContext
    {
        public DbSet<Answer> Answers { get; set; }

        public AnswerContext(DbContextOptions<AnswerContext> options):base()
    }
}