﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestClient.Models
{
    class File
    {
       public File() { }

        public string _id { get; set; }

        public string originalFileName { get; set; }

        public string fileName { get; set; }

        public override string ToString()
        {
            return originalFileName;
        }
    }
}
