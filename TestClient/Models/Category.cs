﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestClient.Models
{
    [Serializable]
    public class Category
    {
        public Category()
        {
            //this.tests = new List<Test>();
        }
        public string _id { get; set; }
        public string name { get; set; }

        public List<Test> tests { get; set; }

        public void AddTest(Test test)
        {
            if (tests == null)
            {
                tests = new List<Test>();
            }
            tests.Add(test);
        }

        public void RemoveTest(int index)
        {
            tests.RemoveAt(index);
        }
        public override string ToString()
        {
            return this.name;
        }
    }
}