﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestClient.Models
{
    public class Question
    {
        public Question()
        {
            answers = new List<Answer>();
        }
        public string _id { get; set; }
        public string name { get; set; }

        // public Answer[] answers { get => listAnswers.ToArray(); set { listAnswers.Clear(); listAnswers.AddRange(value); } }

        public List<Answer> answers { get; set; }
        public bool IsMultiAnswer { get => answers.FindAll(a => a.isTrue).Count > 1; }

        public bool IsValid { get => answers.FindAll(a => a.isTrue).Count > 0 && answers.Count > 0; }

        public bool isClosed { get; set; } = false;

        public void AddAnswer(Answer answer)
        {
            if (answers == null)
            {
                answers = new List<Answer>();
            }
            answers.Add(answer);
        }
        public void RemoveAnswer(int index)
        {
            answers.RemoveAt(index);
        }

        public bool CheckAnswers(List<Answer> answers)
        {
            var trueAnswers = this.answers.FindAll(a => a.isTrue);
            var trueUserAnswers = answers.FindAll(a => trueAnswers.Find(ta => ta.name.ToLower().Trim() == a.name.ToLower().Trim()) != null);

            return trueUserAnswers.Count == trueAnswers.Count && trueUserAnswers.Count == answers.Count;
        }
        public override string ToString()
        {
            return name;
        }
    }
}