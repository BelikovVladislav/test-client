﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestClient.utilites;
using TestClient.Models;

namespace TestClient
{
    public partial class CategoryForm : Form
    {
        private List<Category> categories;

        public CategoryForm()
        {
            InitializeComponent();
        }
        private void syncCategories() 
        {
            categories = Server.GetInstance().GetCategories();
            categoriesBox.ClearSelected();
            categoriesBox.Items.Clear();
            categoriesBox.Items.AddRange(categories.ToArray());
            startTest.Enabled = false;
        }

        private void syncTests()
        {
            testsBox.Items.Clear();
            startTest.Enabled = false;
            var category = (Category)categoriesBox.SelectedItem;
            if (category != null)
            {
                addTestButton.Enabled = Server.GetInstance().IsTeacher;
                var tests = Server.GetInstance().GetTests(category._id);
                if (tests != null)
                {
                    testsBox.Items.AddRange(tests.ToArray());
                }
            }
        }
        private void Category_Load(object sender, EventArgs e)
        {
            addCategoryButton.Enabled = Server.GetInstance().IsTeacher;
            statisticButton.Enabled = Server.GetInstance().IsTeacher;
            addTestButton.Enabled = false;
            startTest.Enabled = false;
            syncCategories();
        }

        private void AddCategoryButton_Click(object sender, EventArgs e)
        {
            AddCategoryForm addCategoryForm = new AddCategoryForm();
            addCategoryForm.ShowDialog();
            syncCategories();
        }

        private void categoriesBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            syncTests();
        }

        private void addTestButton_Click(object sender, EventArgs e)
        {
            var test = new Test();
            test.categoryID = ((Category)categoriesBox.SelectedItem)._id;
            var addTestNameForm = new AddTestNameForm(test);
            addTestNameForm.ShowDialog();
            if (test._id != null && test.IsValid)
            {
                var category = (Category)categoriesBox.SelectedItem;
                category.AddTest(test);
                syncTests();
            }
        }

        private void menuStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void theoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var fileForm = new FIleForm();
            fileForm.ShowDialog();
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void testsBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            startTest.Enabled = testsBox.SelectedItem != null && !Server.GetInstance().IsTeacher;
        }

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void startTest_Click(object sender, EventArgs e)
        {
            var test = (Test)testsBox.SelectedItem;
            var testForm = new TestForm(Server.GetInstance().GetTest(test._id));
            testForm.ShowDialog();
        }

        private void statisticButton_Click(object sender, EventArgs e)
        {
            var globalResult = new GlobalResult();
            globalResult.ShowDialog();
        }
    }
}
