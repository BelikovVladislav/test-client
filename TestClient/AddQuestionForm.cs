﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestClient.Models;
using TestClient.utilites;

namespace TestClient
{
    public partial class AddQuestionForm : Form
    {
        private Question question;
        public AddQuestionForm()
        {
            InitializeComponent();
        }

        public AddQuestionForm(Question question) : this()
        {
            this.question = question;
        }
        private void AddCategoryForm_Load(object sender, EventArgs e)
        {
            nameTextBox.Text = "";
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            if (nameTextBox.Text != "")
            {
                question.name = nameTextBox.Text;
                question.isClosed = isClosedQuestion.Checked;
                Close();
            }
            else
            {
                MessageBox.Show("Введите вопрос");
            }

        }
    }
}
