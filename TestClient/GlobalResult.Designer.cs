﻿namespace TestClient
{
    partial class GlobalResult
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GlobalResult));
            this.periodCB = new System.Windows.Forms.ComboBox();
            this.statisticDGV = new System.Windows.Forms.DataGridView();
            this.period = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.countTest = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.passedCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.percentPassed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.печатьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.statisticDGV)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // periodCB
            // 
            this.periodCB.Dock = System.Windows.Forms.DockStyle.Top;
            this.periodCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.periodCB.FormattingEnabled = true;
            this.periodCB.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.periodCB.Items.AddRange(new object[] {
            "За 7 дней",
            "За месяц",
            "За весь период"});
            this.periodCB.Location = new System.Drawing.Point(0, 28);
            this.periodCB.Name = "periodCB";
            this.periodCB.Size = new System.Drawing.Size(722, 24);
            this.periodCB.TabIndex = 0;
            this.periodCB.TabStop = false;
            this.periodCB.SelectedIndexChanged += new System.EventHandler(this.periodCB_SelectedIndexChanged);
            // 
            // statisticDGV
            // 
            this.statisticDGV.AllowUserToAddRows = false;
            this.statisticDGV.AllowUserToDeleteRows = false;
            this.statisticDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.statisticDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.period,
            this.countTest,
            this.passedCount,
            this.percentPassed});
            this.statisticDGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.statisticDGV.Location = new System.Drawing.Point(0, 52);
            this.statisticDGV.Name = "statisticDGV";
            this.statisticDGV.ReadOnly = true;
            this.statisticDGV.RowHeadersWidth = 51;
            this.statisticDGV.RowTemplate.Height = 24;
            this.statisticDGV.Size = new System.Drawing.Size(722, 398);
            this.statisticDGV.TabIndex = 1;
            // 
            // period
            // 
            this.period.HeaderText = "Период";
            this.period.MinimumWidth = 6;
            this.period.Name = "period";
            this.period.ReadOnly = true;
            this.period.Width = 125;
            // 
            // countTest
            // 
            this.countTest.HeaderText = "Количество тестов";
            this.countTest.MinimumWidth = 6;
            this.countTest.Name = "countTest";
            this.countTest.ReadOnly = true;
            this.countTest.Width = 125;
            // 
            // passedCount
            // 
            this.passedCount.HeaderText = "Количество сдавших";
            this.passedCount.MinimumWidth = 6;
            this.passedCount.Name = "passedCount";
            this.passedCount.ReadOnly = true;
            this.passedCount.Width = 125;
            // 
            // percentPassed
            // 
            this.percentPassed.HeaderText = "Процент сдавших";
            this.percentPassed.MinimumWidth = 6;
            this.percentPassed.Name = "percentPassed";
            this.percentPassed.ReadOnly = true;
            this.percentPassed.Width = 125;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.печатьToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(722, 28);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // печатьToolStripMenuItem
            // 
            this.печатьToolStripMenuItem.Name = "печатьToolStripMenuItem";
            this.печатьToolStripMenuItem.Size = new System.Drawing.Size(72, 24);
            this.печатьToolStripMenuItem.Text = "Печать";
            this.печатьToolStripMenuItem.Click += new System.EventHandler(this.печатьToolStripMenuItem_Click);
            // 
            // GlobalResult
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(722, 450);
            this.Controls.Add(this.statisticDGV);
            this.Controls.Add(this.periodCB);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "GlobalResult";
            this.Text = "Статистика";
            this.Load += new System.EventHandler(this.GlobalResult_Load);
            ((System.ComponentModel.ISupportInitialize)(this.statisticDGV)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox periodCB;
        private System.Windows.Forms.DataGridView statisticDGV;
        private System.Windows.Forms.DataGridViewTextBoxColumn period;
        private System.Windows.Forms.DataGridViewTextBoxColumn countTest;
        private System.Windows.Forms.DataGridViewTextBoxColumn passedCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn percentPassed;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem печатьToolStripMenuItem;
    }
}