﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestClient.Models;
using TestClient.utilites;

namespace TestClient
{
    public partial class TestForm : Form
    {

        private Test test;

        private Dictionary<Question, List<Answer>> statistic;

        private List<List<Control>> controls;

        private bool CanChange = false;
        public TestForm()
        {
            InitializeComponent();
        }

        public TestForm(Test test) : this()
        {
            this.test = test;
            statistic = new Dictionary<Question, List<Answer>>();
            controls = new List<List<Control>>();
            genearateTabs();
        }

        private void genearateTabs()
        {
            for (int index = 0; index < test.questions.Count; index++)
            {
                var q = test.questions[index];
                var label = new Label();
                label.Text = q.name;
                label.Dock = DockStyle.Top;
                var flowLayoutPanel = new TableLayoutPanel();
   //             flowLayoutPanel.AutoSize = true;
   //             flowLayoutPanel.AutoSizeMode = AutoSizeMode.GrowAndShrink;
                flowLayoutPanel.Dock = DockStyle.Bottom;
                flowLayoutPanel.ColumnCount = 2;
                flowLayoutPanel.BackColor = Color.White;
                var tabPage = new TabPage();
                tabPage.BackColor = Color.White;
                tabPage.Text = "Вопрос № " + (index + 1);
                statistic.Add(q, null);
                var controls = new List<Control>();
                if (q.isClosed)
                {
                    var textBox = new TextBox();
                    controls.Add(textBox);
                    flowLayoutPanel.Controls.Add(textBox);
                }
                else
                {
                    foreach (var answer in q.answers)
                    {
                        var checkbox = new CheckBox();
                        checkbox.AutoSize = true;
                        checkbox.Text = answer.name;
                        flowLayoutPanel.Controls.Add(checkbox);
                        controls.Add(checkbox);
                    }
                }
                this.controls.Add(controls);
                tabPage.Controls.Add(label);
                tabPage.Controls.Add(flowLayoutPanel);
                tabControl.TabPages.Add(tabPage);
            }

        }
        private void TestForm_Load(object sender, EventArgs e)
        {
            Text = test.name;
        }

        private void tabControl_Selecting(object sender, TabControlCancelEventArgs e)
        {
            e.Cancel = !CanChange;
        }

        private void nextButton_Click(object sender, EventArgs e)
        {
            var answers = new List<Answer>();
            foreach (var control in controls[tabControl.SelectedIndex])
            {
                if (control is TextBox)
                {
                    var answer = new Answer();
                    answer.name = (control as TextBox).Text;
                    answers.Add(answer);
                }
                else if (control is CheckBox)
                {
                    var checkbox = control as CheckBox;
                    if (checkbox.Checked)
                    {
                        var answer = new Answer();
                        answer.name = checkbox.Text;
                        answers.Add(answer);
                    }
                }
            }
            statistic[test.questions[tabControl.SelectedIndex]] = answers;
            if (tabControl.SelectedIndex == tabControl.Controls.Count - 1)
            {

                var studentStatistic = new StudentStatistic() { test = test, true_questions_count = 0 };

                foreach(var entry in statistic)
                {
                    if (entry.Key.CheckAnswers(entry.Value))
                    {
                        studentStatistic.true_questions_count++;
                    }
                }

                Server.GetInstance().SaveStatistic(studentStatistic);
                var testResult = new TestResult(studentStatistic);
                testResult.ShowDialog();
                Close();
                return;
            }
            CanChange = true;
            tabControl.SelectedIndex++;
            CanChange = false;
            if (tabControl.SelectedIndex == tabControl.Controls.Count - 1)
            {
                nextButton.Text = "Ответить и завершить";
            }
        }
    }
}
