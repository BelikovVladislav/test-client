﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestClient.Models;

namespace TestClient
{
    public partial class TestResult : Form
    {
        public TestResult()
        {
            InitializeComponent();
        }


        public TestResult(StudentStatistic studentStatistic) : this()
        {
            testStatistic.Rows.Add(studentStatistic.test.name, studentStatistic.true_questions_count, studentStatistic.false_questions_count, studentStatistic.percent, studentStatistic.passed ? "Да" : "Нет");
        }
        private void TestResult_Load(object sender, EventArgs e)
        {

        }

        private void печатьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var pd = new PrintDocument();
            pd.PrintPage += (s, eventArgs) =>
            {
                var bmp = new Bitmap(testStatistic.Width, testStatistic.Height);
                testStatistic.DrawToBitmap(bmp, testStatistic.ClientRectangle);
                eventArgs.Graphics.DrawImage(bmp, new Point(100, 100));
            };
            pd.Print();

        }
    }
}
