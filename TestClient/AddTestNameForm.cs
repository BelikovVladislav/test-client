﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestClient.Models;
using TestClient.utilites;

namespace TestClient
{
    public partial class AddTestNameForm : Form
    {
        private Test test;
        public AddTestNameForm()
        {
            InitializeComponent();
        }

        public AddTestNameForm(Test test) : this()
        {
            this.test = test;
        }
        private void AddCategoryForm_Load(object sender, EventArgs e)
        {
            nameTextBox.Text = "";
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            if (nameTextBox.Text != "")
            {
                test.name = nameTextBox.Text;
                var addTest = new AddTestForm(test);
                addTest.ShowDialog();
                Close();
            }
            else
            {
                MessageBox.Show("Введите название");
            }

        }
    }
}
